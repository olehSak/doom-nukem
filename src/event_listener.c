/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event_listener.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dshpack <dshpack@student.unit.ua>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 18:57:00 by dshpack           #+#    #+#             */
/*   Updated: 2019/04/10 18:57:00 by dshpack          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "player_stable.h"

void			check_mouse_pos(int x, int y, int *on_pos)
{
	if (x >= 20 && y >= 65 && x < 90 && y <= 135)
		*on_pos = RECTANGLE;
	else if (x >= 90 && y >= 65 && x < 160 && y <= 135)
		*on_pos = TRIANGLE_DOWN_R;
	else if (x >= 170 && y >= 65 && x < 240 && y <= 135)
		*on_pos = TRIANGLE_DOWN_L;
	else if (x >= 240 && y >= 65 && x < 310 && y <= 135)
		*on_pos = TRIANGLE_UP_R;
	else if (x >= 310 && y >= 65 && x < 380 && y <= 135)
		*on_pos = TRIANGLE_UP_L;
	else
		*on_pos = NONE;
}

void			find_choosen_sector_type(int x, int y, int *type, int *sec_active)
{
	*sec_active = -1;
	if (x >= 20 && y >= 65 && x < 90 && y <= 135 && *type != RECTANGLE)
		*type = RECTANGLE;
	else if (x >= 90 && y >= 65 && x < 160 && y <= 135
			&& *type != TRIANGLE_DOWN_R)
		*type = TRIANGLE_DOWN_R;
	else if (x >= 170 && y >= 65 && x < 240 && y <= 135
			&& *type != TRIANGLE_DOWN_L)
		*type = TRIANGLE_DOWN_L;
	else if (x >= 240 && y >= 65 && x < 310 && y <= 135
			&& *type != TRIANGLE_UP_R)
		*type = TRIANGLE_UP_R;
	else if (x >= 310 && y >= 65 && x < 380 && y <= 135
			&& *type != TRIANGLE_UP_L)
		*type = TRIANGLE_UP_L;
	else
		*type = NONE;
}

void			find_choosen_object(int x, int y, int *type)
{
	if (x >= 120 && y >= 360 && x <= 190 && y <= 430)
		*type = TRUE_PL;
	else if (x >= 210 && y >= 360 && x <= 280 && y <= 430)
		*type = TRUE_ENEMY;
	else if (x >= 85 && y >= 475 && x <= 155 && y <= 545)
		*type = TRUE_START;
	else if (x >= 165 && y >= 475 && x <= 235 && y <= 545)
		*type = TRUE_STAIRWAY;
	else if (x >= 245 && y >= 475 && x <= 315 && y <= 545)
		*type = TRUE_FINISH;
	else if (x >= 30 && y >= 600 && x <= 100 && y <= 670)
		*type = TRUE_HELTH;
	else if (x >= 115 && y >= 600 && x <= 185 && y <= 670)
		*type = TRUE_ARM_SHELL;
	else if (x >= 215 && y >= 600 && x <= 285 && y <= 670)
		*type = TRUE_BODY_ARMOR;
	else if (x >= 300 && y >= 600 && x <= 370 && y <= 670)
		*type = TRUE_HELMET;
	else if (x >= 30 && y >= 720 && x <= 100 && y <= 790)
		*type = TRUE_GUN;
	else if (x >= 115 && y >= 720 && x <= 185 && y <= 790)
		*type = TRUE_MACHINE_GUN;
	else if (x >= 215 && y >= 720 && x <= 285 && y <= 790)
		*type = TRUE_BULLETS;
	else if (x >= 300 && y >= 720 && x <= 370 && y <= 790)
		*type = TRUE_MAGAZINE;
	else
		*type = NO_OBJ;
}

static void		mouse_evnt(t_env *env, SDL_Event event, t_eflags *flags)
{
	if (event.type == SDL_MOUSEMOTION)
	{
		flags->curr_pos.x = event.motion.x;
		flags->curr_pos.y = event.motion.y;
		check_mouse_pos(flags->curr_pos.x, flags->curr_pos.y, &flags->on_pos);
	}
	if (event.type == SDL_MOUSEBUTTONDOWN)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
		{
			flags->mouse_pos.x = event.motion.x;
			flags->mouse_pos.y = event.motion.y;
			flags->l_mouse_down = TRUE;
			if (flags->mouse_pos.x >= 20 && flags->mouse_pos.x <= 380 &&
				flags->mouse_pos.y >= 65 && flags->mouse_pos.y <= 135)
				find_choosen_sector_type(flags->mouse_pos.x, flags->mouse_pos.y,
					&flags->sec.type_btn, &flags->sec.active);
			else if (flags->mouse_pos.x <= WIREFRAME_W &&
					flags->mouse_pos.y <= WIREFRAME_H &&
					flags->mouse_pos.x >= OFFSET_X &&
					flags->mouse_pos.y >= OFFSET_Y)
			{
				if (env->map.dot[CALC_Y(flags->mouse_pos.y)][CALC_X(flags->mouse_pos.x)].status > -1 &&
					flags->sec.type_btn == -1 && flags->sec.object_btn == -1)
					flags->sec.active = env->map.dot[CALC_Y(flags->mouse_pos.y)][CALC_X(flags->mouse_pos.x)]
							.sect_num;
				else if (flags->sec.type_btn > -1 && flags->sec.object_btn == -1 && flags->sec.active == -1)
					flags->sec.create_sec = TRUE;
				else if (flags->sec.type_btn == -1 && flags->sec.object_btn > -1 && flags->sec.active > -1)
					flags->sec.create_obj = TRUE;
			}
			else if (flags->sec.type_btn == NONE && flags->sec.active > -1 && flags->mouse_pos.x >= 30 &&
					flags->mouse_pos.x <= 370 && flags->mouse_pos.y >= 360 && flags->mouse_pos.y <= 790)
				find_choosen_object(flags->mouse_pos.x, flags->mouse_pos.y, &flags->sec.object_btn);
			else if (flags->mouse_pos.x >= 130 && flags->mouse_pos.x <= 273 &&
					flags->mouse_pos.y >= 1120 && flags->mouse_pos.y <= 1173)
				flags->game_start = TRUE;
			else
				flags->sec.active = -1;
			event_arrows_handler_parameters(&env->sdata, flags);
		}
	}
	if (event.type == SDL_MOUSEBUTTONUP)
	{
		if (event.button.button == SDL_BUTTON_LEFT)
			flags->l_mouse_down = FALSE;
	}
}

static void		keyboard_event(SDL_Event event, SDL_Keycode key, t_eflags *flags)
{
	if (event.type == SDL_KEYDOWN)
	{
		if (key == SDLK_ESCAPE)
		{
			flags->sec.active = -1;
			flags->sec.type_btn = NONE;
			flags->sec.create_sec = FALSE;
			flags->sec.object_btn = NO_OBJ;
		}
		if (key == SDLK_DELETE && flags->sec.active > -1)
			flags->btn_delete = 1;
	}
}

void			event_listener(t_eflags *flags, t_env *env)
{
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			flags->running = 0;
		if (event.type == SDL_WINDOWEVENT)
			if (event.window.event == SDL_WINDOWEVENT_CLOSE)
				if (SDL_GetWindowID(env->graph->window) == event.window.windowID)
					flags->running = 0;
		mouse_evnt(env, event, flags);
		keyboard_event(event, event.key.keysym.sym, flags);
	}
}
