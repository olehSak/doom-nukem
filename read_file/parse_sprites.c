 /* ************************************************************************** */
 /*                                                                            */
 /*                                                        :::      ::::::::   */
 /*   parse_sprites.c                                    :+:      :+:    :+:   */
 /*                                                    +:+ +:+         +:+     */
 /*   By: olaktion <marvin@42.fr>                    +#+  +:+       +#+        */
 /*                                                +#+#+#+#+#+   +#+           */
 /*   Created: 2019/06/25 14:01:47 by olaktion          #+#    #+#             */
 /*   Updated: 2019/06/25 14:01:49 by olaktion         ###   ########.fr       */
 /*                                                                            */
 /* ************************************************************************** */

 #include "player_stable.h"

 void			parse_sprites(t_system *system, t_list *list)
 {
 	t_list		*spr;

 	spr = find_elem(list, "sprite");
 	system->sprite = parse_sprite_param((char *)spr->content, system);
 }

 t_sprite		parse_sprite_param(char *data, t_system *system)
 {
 	char		**tmp;
 	t_sprite	sprite;

 	ft_bzero(&sprite, sizeof(t_sprite));
 	if (data)
 	{
 		tmp = ft_strsplit(ft_strchr(data, '\t'), '\t');
 		if (two_len(tmp) == 2)
 		{
 			sprite.where = def_pos(tmp[M1(1)]);
 			if (!int_error(&tmp[M1(2)]))
 				sprite.num_sect = ft_atoi(tmp[M1(2)]);
 		}
 		else
 			print_error("Error, bad sprite position");
 		two_del(&tmp);
 	}
 }

 t_abc			def_pos(char *data)
 {
 	t_abc		pos;
 	char		**tmp;

 	ft_bzero(&pos, sizeof(t_abc));
 	if (data)
 	{
 		tmp = ft_strsplit(data, ' ');
 		if (two_len(tmp) == 3 && !int_error(tmp))
 		{
 			pos.a = ft_atoi(tmp[M1(1)]);
 			pos.b = ft_atoi(tmp[M1(2)]);
 			pos.c = ft_atoi(tmp[M1(3)]);
 		}
 		else
 			print_error("Error bad coords for sprite");
 		two_del(&tmp);
 	}
 	return (pos);
 }
